package no.uib.inf101.sem2.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class WordFactory {

    // Alt for klasse 3

    private static ArrayList<Word> all4WordClassesList = new ArrayList<>();

    private static final String NOUNS_FILE = "/Users/theodorandreasen/Desktop/Haakon/swing-learn-words/src/main/java/no/uib/inf101/sem2/substantiv.txt";
    private static final String VERBS_FILE = "/Users/theodorandreasen/Desktop/Haakon/swing-learn-words/src/main/java/no/uib/inf101/sem2/verb.txt";
    private static final String ADJECTIVES_FILE = "/Users/theodorandreasen/Desktop/Haakon/swing-learn-words/src/main/java/no/uib/inf101/sem2/Adjektiv.txt";
    private static final String PREPOSITIONS_FILE = "/Users/theodorandreasen/Desktop/Haakon/swing-learn-words/src/main/java/no/uib/inf101/sem2/preposisjoner.txt";
    private static final Random random = new Random();


    private static ArrayList<String> readWordsFromFile(String fileName) throws IOException {
        ArrayList<String> wordListFromFile = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
        String line = reader.readLine();
        while (line != null) {
            wordListFromFile.add(line.trim());
            line = reader.readLine();
        }
        reader.close();
        return wordListFromFile;
    }

    public static Word getRandomNoun() throws IOException {
        ArrayList<String> words = readWordsFromFile(NOUNS_FILE);
        String randomWord = words.get(random.nextInt(words.size()));
        return new Word(randomWord, WordClass.SUBSTANTIV);
    }

    public static Word getRandomVerb() throws IOException {
        ArrayList<String> words = readWordsFromFile(VERBS_FILE);
        String randomWord = words.get(random.nextInt(words.size()));
        return new Word(randomWord, WordClass.VERB);
    }

    public static Word getRandomAdjective() throws IOException {
        ArrayList<String> words = readWordsFromFile(ADJECTIVES_FILE);
        String randomWord = words.get(random.nextInt(words.size()));
        return new Word(randomWord, WordClass.ADJEKTIV);
    }

    public static Word getRandomPreposition() throws IOException {
        ArrayList<String> words = readWordsFromFile(PREPOSITIONS_FILE);
        String randomWord = words.get(random.nextInt(words.size()));
        return new Word(randomWord, WordClass.PREPOSISJON);
    }


    // Denne metoden kaller på alle metodene over én gang. Må ha så omfattende for IO Exception. Automatisk generert.
    private static void addWordsDifferentWordclasses() {
        try {
            all4WordClassesList.add(getRandomPreposition());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            all4WordClassesList.add(getRandomNoun());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            all4WordClassesList.add(getRandomAdjective());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            all4WordClassesList.add(getRandomVerb());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Metoden henter ut et random ord av listen som random er blitt opprettet, som består av et ord fra hver ordklasse.
    // Metoden kaller først på metoden som lager listen med fire ord, 
    // omrokkerer listen, og velger ordet som havner på index 0.
    public static Word getRandomWord() {
        addWordsDifferentWordclasses();
        Collections.shuffle(all4WordClassesList);
        return all4WordClassesList.get(0);
    }
 

    // Alt for klasse 1

    private static ArrayList<Word> wordsAndSymbols = new ArrayList<>();

    private static final String WordList_FILE = "/Users/theodorandreasen/Desktop/Haakon/swing-learn-words/src/main/java/no/uib/inf101/sem2/WordListKlasse1.txt";
    private static final String SymbolList_FILE = "/Users/theodorandreasen/Desktop/Haakon/swing-learn-words/src/main/java/no/uib/inf101/sem2/SymbolListKlass1.txt";

        
    private static ArrayList<String> readWordsAndSymbolsFromFile(String fileName) throws IOException {
        ArrayList<String> wordListFromFile = new ArrayList<>();
        // Må bruke UTF-8 encoding for at emojiene og norske bokstaver skal vises.
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
        String line = reader.readLine();
        while (line != null) {
            wordListFromFile.add(line.trim());
            line = reader.readLine();
        }
        reader.close();
        return wordListFromFile;
    }

    public static Word getRandomSymbolAndWord() throws IOException {
        ArrayList<String> words = readWordsAndSymbolsFromFile(WordList_FILE);
        ArrayList<String> symbols = readWordsAndSymbolsFromFile(SymbolList_FILE);

        Random random = new Random();
        int index = random.nextInt(words.size());

        String randomWord = words.get(index);
        String randomSymbol = symbols.get(index);

        return new Word(randomSymbol, randomWord);
    }


    private static void addSymbolAndWord() {
        try {
            wordsAndSymbols.add(getRandomSymbolAndWord());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            wordsAndSymbols.add(getRandomSymbolAndWord());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            wordsAndSymbols.add(getRandomSymbolAndWord());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Word> getSymbolAndWord(int number) {
        addSymbolAndWord();
        Collections.shuffle(wordsAndSymbols);
        ArrayList<Word> randomWords = new ArrayList<>(wordsAndSymbols.subList(0, number));
        return randomWords;
    }
}


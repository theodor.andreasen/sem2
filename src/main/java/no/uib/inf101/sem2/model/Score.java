package no.uib.inf101.sem2.model;

public class Score {
    private int points;
    private int lives;
    private int xCount;
    private int yCount;

    public Score(int lives) {
        points = 0;
        this.lives = lives;
        xCount = lives;
        yCount = 0;
    }

    public int getPoints() {
        return points;
    }

    public void incrementScore(int points) {
        this.points += points;
    }

    public int getLives() {
        return lives;
    }

    public void loseOneLife() {
        lives--;
        if (xCount > 0) {
            xCount--;
            yCount++;
        }
    }

    public int getXCount() {
        return xCount;
    }

    public int getYCount() {
        return yCount;
    }
}

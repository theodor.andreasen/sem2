package no.uib.inf101.sem2.model;

public class Word {

    private String symbol;
    private String word;
    private WordClass wordClass;

    // different constructors for different uses

    public Word(String word, WordClass wordClass) {
        this.word = word;
        this.wordClass = wordClass;
    }

    public Word(String symbol, String word) {
        this.symbol = symbol;
        this.word = word;
    }

    public String getSymbol() {
        return symbol;
    }

    // bør kanskje hete noe annet
    public String getWord() {
        return word;
    }

    public WordClass getWordClass() {
        return wordClass;
    }

    public boolean checkWord(String word) {
        return word.equals(this.word);
    }

    public static void main(String[] args) {
        Word icon = new Word("⛵", "båt");
        System.out.println(icon.getWord());
    }

}

package no.uib.inf101.sem2.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import no.uib.inf101.sem2.model.GameState;

public class BarneskoleFrame extends JFrame {
    GameState gameState = GameState.ACTIVE_GAME;

    private JLabel currentWord;
    private JLabel score;
    private JLabel xLabel1;
    private JLabel xLabel2;
    private JLabel xLabel3;
    private JLabel timeLabel;

    private Timer timer;
    private int timeLeft;

    public BarneskoleFrame(String word) {

        setSize(800, 400);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Load the Twinkle Twinkle Little Star melody
        Sequence sequence = null;
        try {
            sequence = MidiSystem.getSequence(new File("twinkle.mid"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Start the sequencer and play the melody
        Sequencer sequencer = null;
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequencer.setSequence(sequence);
            sequencer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JPanel wordClassPanel = new JPanel(new BorderLayout());

        JLabel labelNorth = new JLabel("Substantiv (trykk ⬆️)");
        JLabel labelSouth = new JLabel("Adjektiv (trykk ⬇️");
        JLabel labelEast = new JLabel("Verb (trykk ➡️)");
        JLabel labelWest = new JLabel("Preposisjon (trykk ⬅️");

        xLabel1 = new JLabel("❤️");
        xLabel1.setFont(new Font("SansSerif", Font.BOLD, 30));
        xLabel1.setForeground(Color.RED);

        xLabel2 = new JLabel("❤️");
        xLabel2.setFont(new Font("SansSerif", Font.BOLD, 30));
        xLabel2.setForeground(Color.RED);

        xLabel3 = new JLabel("❤️ ");
        xLabel3.setFont(new Font("SansSerif", Font.BOLD, 30));
        xLabel3.setForeground(Color.RED);

        JPanel heartsPanel = new JPanel();
        JLabel livLabel = new JLabel("Liv:");
        heartsPanel.add(livLabel);
        heartsPanel.add(xLabel1);
        heartsPanel.add(xLabel2);
        heartsPanel.add(xLabel3);
        heartsPanel.setBackground(Color.GREEN);
        
        currentWord = new JLabel(word);
        currentWord.setHorizontalAlignment(JLabel.CENTER);
        currentWord.setVerticalAlignment(JLabel.CENTER);
        currentWord.setFont(new Font("SansSerif", Font.BOLD, 30));
        currentWord.setForeground(Color.BLACK);
        currentWord.setOpaque(true);
        currentWord.setPreferredSize(new Dimension(5, 5));

        labelNorth.setHorizontalAlignment(JLabel.CENTER);
        labelSouth.setHorizontalAlignment(JLabel.CENTER);

        wordClassPanel.add(labelNorth, BorderLayout.NORTH);
        wordClassPanel.add(labelSouth, BorderLayout.SOUTH);
        wordClassPanel.add(labelEast, BorderLayout.EAST);
        wordClassPanel.add(labelWest, BorderLayout.WEST);
        wordClassPanel.add(currentWord);

        JPanel scoreboardPanel = new JPanel();
        score = new JLabel("Score: 0");

  
        timeLabel = new JLabel("Time left: 60s");
        timeLabel.setForeground(Color.BLUE);
        timeLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
        
        JLabel instructionLabel = new JLabel("Putt ordene i riktig ordklasse😊");
        instructionLabel.setForeground(Color.BLACK);
        instructionLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
        
        JPanel timePanel = new JPanel(new BorderLayout());
        timePanel.add(timeLabel, BorderLayout.EAST);
        timePanel.add(instructionLabel, BorderLayout.WEST);
        timePanel.setBackground(Color.GREEN);
        timePanel.setPreferredSize(new Dimension(getWidth(), 50));
        
        scoreboardPanel.add(score);
        scoreboardPanel.setBackground(Color.GREEN);
        scoreboardPanel.setPreferredSize(new Dimension(getWidth(), 50));

        // add panels
        add(wordClassPanel, BorderLayout.CENTER);
        add(scoreboardPanel, BorderLayout.SOUTH);
        add(heartsPanel, BorderLayout.WEST);
        add(timePanel, BorderLayout.NORTH);
    
        // start the timer
        timeLeft = 60;
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                timeLeft--;
                if (timeLeft >= 0) {
                    timeLabel.setText("Time left: " + timeLeft + "s");
                } else {
                    gameState = GameState.GAME_OVER;
                    ((Timer) e.getSource()).stop();
                    currentWord.setText("Tiden er ute!");
                    
                }
            }
        });
        timer.start();
    }
    

    public void setWord(String word) {
        this.currentWord.setText(word);
    }

    public void setScore(int score) {
        this.score.setText("Score: " + score);
    }

    public void setLives(int lives) {
        if (lives == 2) {
            xLabel1.setText("☠️");
            xLabel1.setForeground(Color.BLACK);
            xLabel1.setFont(new Font("SansSerif", Font.BOLD, 30));

    
        } else if (lives == 1) {
            xLabel2.setText("☠️");
            xLabel2.setForeground(Color.BLACK);
            xLabel2.setFont(new Font("SansSerif", Font.BOLD, 30));

        } else if (lives == 0) {
            xLabel3.setText("☠️");
            xLabel3.setForeground(Color.BLACK);
            xLabel3.setFont(new Font("SansSerif", Font.BOLD, 30));
 // stop the timer
 timer.stop();
 // set game state to game over
 gameState = GameState.GAME_OVER;
        }
    }


    public void setScoreText(String string) {
    }
    
    
    
}


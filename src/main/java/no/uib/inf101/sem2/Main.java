package no.uib.inf101.sem2;


import no.uib.inf101.sem2.view.Welcomepage;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    //SampleView view = new SampleView();
    Welcomepage welcomepage = new Welcomepage();

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101");
    frame.setContentPane(welcomepage);
    frame.pack();
    frame.setVisible(true);
  }
}

package no.uib.inf101.sem2.controller;

import java.util.ArrayList;

import no.uib.inf101.sem2.model.Word;
import no.uib.inf101.sem2.model.WordFactory;
import no.uib.inf101.sem2.view.BarnehageFrame;

public class BarnehageKontroller {

    public BarnehageKontroller() {

        ArrayList<Word> words = WordFactory.getSymbolAndWord(3);
        new BarnehageFrame(words);
    }
    
}
package no.uib.inf101.sem2.controller;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import no.uib.inf101.sem2.model.Score;
import no.uib.inf101.sem2.model.Word;
import no.uib.inf101.sem2.model.WordClass;
import no.uib.inf101.sem2.model.WordFactory;
import no.uib.inf101.sem2.view.BarneskoleFrame;

public class BarneskoleKontroller {

    private BarneskoleFrame frame;
    private Score score;
    private Word currentWord;
    private Word lastWord;
    private int lives = 3;

    public BarneskoleKontroller() {
        currentWord = WordFactory.getRandomWord();
        lastWord = currentWord;

        frame = new BarneskoleFrame(currentWord.getWord());

        frame.addKeyListener(new Klasse3KeyListener());

        score = new Score(lives);

        // add scoreboard
        // when key pressed, logic is done
        // and scoreboard may be updated (in model and view)
    }

    private void correctKeyPressedAction() {
        // avoid picking same word twice
        lastWord = currentWord;
        while (currentWord.getWord().equals(lastWord.getWord())) {
            currentWord = WordFactory.getRandomWord();
        }
        frame.setWord(currentWord.getWord());
        score.incrementScore(10);
        frame.setScore(score.getPoints());
    }

    private void wrongKeyPressedAction() {
        lives--;
        if (lives < 0) {
            
            
            
           
      
            frame.setWord("Ingen flere liv!");
            frame.removeKeyListener(frame.getKeyListeners()[0]);
        }
        else {
            score.loseOneLife();
            frame.setLives(score.getXCount());
        }
    }

   
    private class Klasse3KeyListener implements KeyListener {

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    if (currentWord.getWordClass() == WordClass.SUBSTANTIV) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    
                    }
                    break;

                case KeyEvent.VK_DOWN:
                    if (currentWord.getWordClass() == WordClass.ADJEKTIV) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                case KeyEvent.VK_LEFT:
                    if (currentWord.getWordClass() == WordClass.PREPOSISJON) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                case KeyEvent.VK_RIGHT:
                    if (currentWord.getWordClass() == WordClass.VERB) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                default:
                    break;
            }
        }

        @Override
        public void keyReleased(KeyEvent arg0) {
            // do nothing
        }

        @Override
        public void keyTyped(KeyEvent arg0) {
            // do nothing
        }

    }
}